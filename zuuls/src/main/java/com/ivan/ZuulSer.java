package com.ivan;

import com.ivan.Filter.MyFilter;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.context.annotation.Bean;

/**
 * @author 11900
 * @version V1.0
 * @Title: ${FILE_NAME}
 * @Package com.ivan
 * @Description: TODO
 * @date 2019/6/4 15:05
 */
@EnableZuulProxy
@SpringBootApplication
public class ZuulSer {
     public static void main(String[] args) throws Exception {
         SpringApplication.run(ZuulSer.class,args);
     }
    //将过滤器交给Spring管理
    @Bean
    public MyFilter tokenFilter(){
        return new MyFilter();
    }
}
