package com.ivan;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

/**
 * @author 11900
 * @version V1.0
 * @Title: ${FILE_NAME}
 * @Package com.ivan
 * @Description: TODO
 * @date 2019/6/4 11:13
 */
@EnableEurekaServer
@SpringBootApplication
public class EurekaServerApplication {
     public static void main(String[] args) throws Exception {
         SpringApplication.run( EurekaServerApplication.class, args );
     }
}
