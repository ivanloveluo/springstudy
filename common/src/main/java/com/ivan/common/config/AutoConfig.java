package com.ivan.common.config;

import com.ivan.common.intercepter.AuthIntercepter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * @author 11900
 * @version V1.0
 * @Title: ${FILE_NAME}
 * @Package com.ivan.config
 * @Description: TODO
 * @date 2019/6/18 14:30
 * 拦截自动配置
 */
@Configuration
public class AutoConfig  extends WebMvcConfigurerAdapter {
    @Bean
    public AuthIntercepter addAuthIntercepter(){
        return   new AuthIntercepter();
    }
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        AuthIntercepter authIntercepter = addAuthIntercepter();
        registry.addInterceptor(authIntercepter);
        super.addInterceptors(registry);
    }
}
