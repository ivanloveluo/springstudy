package com.ivan.common.intercepter;

import com.ivan.common.context.FilterContextHandler;
import com.ivan.common.dto.UserToken;
import com.ivan.common.utils.JwtUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.lang.Nullable;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author 11900
 * @version V1.0
 * @Title: ${FILE_NAME}
 * @Package com.ivan.common
 * @Description: TODO
 * @date 2019/6/5 10:41
 * 服务前调用APPID和被调用服务appid相比对
 *
 */
public class AuthIntercepter extends HandlerInterceptorAdapter {
    private static final Logger logger = LoggerFactory.getLogger(AuthIntercepter.class);

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String requestURI = request.getRequestURI();
        System.out.println(request.getRequestURL()+"--------------requestURI=============30");
        String appid = request.getHeader("appid");
        logger.info("get----appid------"+appid+"-"+Thread.currentThread().getId());
       // String token = request.getHeader(CommonConstants.CONTEXT_TOKEN);
        UserToken userToken_temp = new UserToken("zs", "123", "zs");
         String token = JwtUtils.generateToken(userToken_temp, 2 * 60 * 60 * 1000);
         UserToken userToken = JwtUtils.getInfoFromToken(token);
        System.out.println(userToken+"------------");
        if(HasAuth(appid)){
            FilterContextHandler.setToken(token);
            logger.info("------设置token"+Thread.currentThread().getId());
            FilterContextHandler.setUsername(userToken.getUsername());
            FilterContextHandler.setName(userToken.getName());
            FilterContextHandler.setUserID(userToken.getUserId());
            return super.preHandle(request, response, handler);
        }else {
            return  false;
        }
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, @Nullable Exception ex) throws Exception {
        super.afterCompletion(request, response, handler, ex);
    }

    public  boolean HasAuth(String  appid){
        // 调用admin 服务 验证appid 是否合法
        //   且设置 访问  admin 服务 验证appid  服务时免验证
        return true;
    }
}
