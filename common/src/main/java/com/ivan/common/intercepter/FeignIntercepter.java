package com.ivan.common.intercepter;

import feign.RequestInterceptor;
import feign.RequestTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

/**
 * @author 11900
 * @version V1.0
 * @Title: ${FILE_NAME}
 * @Package com.ivan.common
 * @Description: TODO
 * @date 2019/6/4 19:28
 */
@Configuration
public class FeignIntercepter implements RequestInterceptor {
    private static final Logger logger = LoggerFactory.getLogger(RequestInterceptor.class);
    @Value("${spring.application.name}")
    private  String sname;

    @Override
    public void apply(RequestTemplate requestTemplate) {
        logger.info("------feign设置token,只为在请求头中添加相关信息，" + Thread.currentThread().getId());
        logger.info("requestTemplate.url()=="+requestTemplate.url());
        logger.info("appid==="+sname);
        // 想起他服务发起调用时发生 验证本服务是否合法  传染本服务用户名密码 获取token放入其中
        requestTemplate.header("appid",sname);

    }
}
