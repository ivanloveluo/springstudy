package com.ivan.common.utils;

import com.ivan.common.constants.CommonConstants;
import com.ivan.common.dto.UserToken;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import java.util.Date;

/**
 * @author bootdo 1992lcg@163.com
 * @version V1.0
 */
public class JwtUtils {
    public static String generateToken(UserToken userToken, int expire) throws Exception {
        String token = Jwts.builder()
                .setSubject(userToken.getUsername())
                .claim(CommonConstants.CONTEXT_USER_ID, userToken.getUserId())
                .claim(CommonConstants.CONTEXT_NAME, userToken.getName())
                .claim(CommonConstants.RENEWAL_TIME,new Date(System.currentTimeMillis()+expire/2))
                .setExpiration(new Date(System.currentTimeMillis()+expire))
                .signWith(SignatureAlgorithm.HS256, CommonConstants.JWT_PRIVATE_KEY)
                .compact();
        return token;
    }


    public static UserToken getInfoFromToken(String token) throws Exception {
        Claims claims = Jwts.parser()
                .setSigningKey(CommonConstants.JWT_PRIVATE_KEY).parseClaimsJws(token)
                .getBody();
        return new UserToken(claims.getSubject(), claims.get(CommonConstants.CONTEXT_USER_ID).toString(),claims.get(CommonConstants.CONTEXT_NAME).toString());
    }

    public  static  void main(String[] arges){
        UserToken userToken = new UserToken("zs", "123", "zs");
        try {
            String s = generateToken(userToken, 2 * 60 * 60 * 1000);
            System.out.println("TOKEN=="+s);
            UserToken infoFromToken = getInfoFromToken(s);
            System.out.println("infoFromToken="+infoFromToken.toString());
        }catch (io.jsonwebtoken.SignatureException ex){
            System.out.println("io.jsonwebtoken.SignatureException" );
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
