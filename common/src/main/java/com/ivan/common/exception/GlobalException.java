package com.ivan.common.exception;

/**
 * @author 11900
 * @version V1.0
 * @Title: ${FILE_NAME}
 * @Package com.ivan.common.exception
 * @Description: TODO
 * @date 2019/6/26 9:53
 */
public class GlobalException  extends Exception{
    private int code;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }
    public GlobalException(GlobalException e) {
        super(e.getMessage());
        this.code = e.getCode();
    }

    public GlobalException(String message) {
        super(message);
    }


    public GlobalException(String message, int code) {
        super(message);
        this.code = code;
    }
}
