package com.ivan.common.test.thread;

/**
 * @author 11900
 * @version V1.0
 * @Title: ${FILE_NAME}
 * @Package com.ivan.common.test.thread
 * @Description: TODO
 * @date 2019/7/10 10:53
 */
public class MyFyTask implements Runnable {

    private String  dataSources=null;//数据源
    private int taskNum=0;

    public MyFyTask(int num,String dataSources) {
        this.taskNum = num;
        this.dataSources= dataSources;
    }

    @Override
    public void run() {
        System.out.println("法院"+taskNum+"正在执行task ----------------线程名称"+Thread.currentThread().getName());
        try {
            Thread.currentThread().sleep(4000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
      //  System.out.println("法院"+taskNum+"执行task完成+++++++线程名称"+Thread.currentThread().getName());
    }
}
