package com.ivan.common.test.io.nio;

import java.nio.ByteBuffer;

/**
 * @author 11900
 * @version V1.0
 * @Title: ${FILE_NAME}
 * @Package com.ivan.common.test.io.nio
 * @Description: TODO
 * @date 2019/7/9 9:43
 */
public class TestBuffer {
     public static void main(String[] args) throws Exception {
         String str="ivan";
         //分配一个指定大小的缓冲区
         ByteBuffer buffer = ByteBuffer.allocate(1024);
         System.out.println("-------allocate创建-------");
         printBufferInfor(buffer);
         //put缓存数据
         System.out.println("-------put缓存数据-------");
         buffer.put(str.getBytes());
         printBufferInfor(buffer);
         //切换读模式
         System.out.println("-------切换读模式-------");
         buffer.flip();
         printBufferInfor(buffer);
         //4. 利用 get() 读取缓冲区中的数据
         System.out.println("-------读取缓冲区中的数据-------");
         byte[] dst = new byte[buffer.limit()];
         buffer.get(dst);
         System.out.println(new String(dst, 0, dst.length));

         //5. rewind() : 可重复读
         buffer.rewind();
         System.out.println("-----------------rewind() 可重复读----------------");
         printBufferInfor(buffer);
         //6. clear() : 清空缓冲区. 但是缓冲区中的数据依然存在，但是处于“被遗忘”状态
         buffer.clear();

         System.out.println("-----------------clear()----------------");
         printBufferInfor(buffer);
         System.out.println("-------清空缓冲区读取缓冲区中的数据-------");
         byte[] bytes = buffer.array();
         System.out.println(bytes[2]);
         System.out.println("====处理器核数======");
         System.out.println(Runtime.getRuntime().availableProcessors());

     }
     public static void printBufferInfor(ByteBuffer buffer ){
         System.out.println("position=" + buffer.position());
         System.out.println("limit=" + buffer.limit());
         System.out.println("capacity=" + buffer.capacity());
     }

}
