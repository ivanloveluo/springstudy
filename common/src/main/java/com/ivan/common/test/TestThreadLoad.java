package com.ivan.common.test;

/**
 * @author 11900
 * @version V1.0
 * @Title: ${FILE_NAME}
 * @Package com.ivan.common.test
 * @Description: TODO
 * @date 2019/6/28 10:12
 */
public class TestThreadLoad {
    private  static ThreadLocal<String> tc = new ThreadLocal<>();
    public static void main(String[] args) throws Exception {
        Thread thread = Thread.currentThread();
        tc.set("ch");
        System.out.println(tc.get());
    }
}
