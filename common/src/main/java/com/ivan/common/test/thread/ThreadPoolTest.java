package com.ivan.common.test.thread;

import java.util.concurrent.*;

/**
 * @author 11900
 * @version V1.0
 * @Title: ${FILE_NAME}
 * @Package com.ivan.common.test.thread
 * @Description: TODO
 * @date 2019/7/10 10:32
 * 自定义线程池技术
 * 拒绝策略
maximumPoolSize:指定了线程池中的最大线程数量，这个参数会根据你使用的workQueue任务队列的类型，决定线程池会开辟的最大线程数量；

keepAliveTime:当线程池中空闲线程数量超过corePoolSize时，多余的线程会在多长时间内被销毁；

unit:keepAliveTime的单位

workQueue:任务队列，被添加到线程池中，但尚未被执行的任务；它一般分为直接提交队列、有界任务队列、无界任务队列、优先任务队列几种；

threadFactory:线程工厂，用于创建线程，一般用默认即可；

handler:拒绝策略；当任务太多来不及处理时，如何拒绝任务；
 */
public class ThreadPoolTest {
    private static ExecutorService pool;
     public static void main(String[] args) throws Exception {
         /**
          * LinkedBlockingQueue:一个基于链表结构的阻塞队列,此队列按FIFO排序元素,吞吐量通常要高于ArrayBlockingQueue.静态工厂方法Executors.newFixedThreadPool()使用了这个队列
          SynchronousQueue:一个不存储元素的阻塞队列.每个插入操作必须等到另一个线程调用移除操作,否则插入操作一直处于阻塞状态,吞吐量通常要高于Linked-BlockingQueue,静态工厂方法Executors.newCachedThreadPool使用了这个队列
          ThreadPoolExecutor.AbortPolicy:丢弃任务并抛出RejectedExecutionException异常。
          ThreadPoolExecutor.DiscardPolicy：也是丢弃任务，但是不抛出异常。
          ThreadPoolExecutor.DiscardOldestPolicy：丢弃队列最前面的任务，然后重新尝试执行任务（重复此过程）
          ThreadPoolExecutor.CallerRunsPolicy：由调用线程处理该任务
          */
         pool = new ThreadPoolExecutor(4, 8, 1000, TimeUnit.MILLISECONDS, new LinkedBlockingQueue<Runnable>(5),  new ThreadFactory() { //ThreadFactory自定义线程创建 线程池中线程就是通过ThreadPoolExecutor中的ThreadFactory，线程工厂创建的。那么通过自定义ThreadFactory，可以按需要对线程池中创建的线程进行一些特殊的设置，如命名、优先级等，下面代码我们通过ThreadFactory对线程池中创建的线程进行记录与命名
             @Override
             public Thread newThread(Runnable r) {
                  //线程命名
                 Thread th = new Thread(r,"threadPool"+r.hashCode());
                 return th;
             }
         }, new ThreadPoolExecutor.CallerRunsPolicy());
        // ExecutorService executorService = Executors.newFixedThreadPool(4);
         while (true){
             Thread.sleep(1000);
             for(int i=1;i<10;i++){
                 MyFyTask myTask = new MyFyTask(i,"数据源");
                 pool.execute(myTask);
             }
         }

     }

}
