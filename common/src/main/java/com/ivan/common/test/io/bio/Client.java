package com.ivan.common.test.io.bio;

import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.Random;

/**
 * @author 11900
 * @version V1.0
 * @Title: ${FILE_NAME}
 * @Package com.ivan.common.test.io.bio
 * @Description: TODO
 * @date 2019/7/4 10:57
 */
public class Client {
     public static void main(String[] args) throws Exception {
         for (int i = 0; i < 20; i++) {
             Socket socket = new Socket();
             socket.connect(new InetSocketAddress("localhost",8089));
             processWithNewThread(socket,i);
         }

     }
     static void processWithNewThread(Socket s, int i){
         Runnable runnable =()->{
            try {
                //睡眠随机的5-10秒,模拟数据尚未就绪
                Thread.sleep((new Random().nextInt(6)+5)*1000);
                //写1M数据，为了拉长服务器端读数据的过程
                s.getOutputStream().write(prepareBytes());
                //睡眠1秒，让服务器端把数据读完
                Thread.sleep(1000);
                s.close();
            }catch (Exception e){

            }
         };
         new Thread(runnable).start();
     }
     static byte[] prepareBytes(){
         byte[] bytes = new byte[1024 * 1024 * 1];
         for (int i = 0; i < bytes.length; i++) {
             bytes[i]=1;
         }
         return bytes;
     }
}
