package com.ivan.controller;

import com.ivan.api.OrderServiceHi;
import com.netflix.discovery.EurekaClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author 11900
 * @version V1.0
 * @Title: ${FILE_NAME}
 * @Package com.ivan.conller
 * @Description: TODO
 * @date 2019/6/4 12:43
 */
@RestController
public class MemberController {
    @Value("${server.port}")
    String port;

    @Autowired
    private EurekaClient eurekaClient;

    @RequestMapping("/me")
    public String me(@RequestParam(value = "name", defaultValue = "forezp") String name) {
        return "hi " + name + " ,i am from port:" + port+" , 这是会员服务 memberserver";
    }
    //编译器报错，无视。 因为这个Bean是在程序启动的时候注入的，编译器感知不到，所以报错。
    @Autowired
    OrderServiceHi orderServiceHi;

    @GetMapping(value = "/hi")
    public String sayHi(@RequestParam String name) {
        return orderServiceHi.sayHiFromClientOne( name );
    }

    @GetMapping("/infor")
    public Object getInfor(){
        return eurekaClient.getInstancesByVipAddress("server-member",false);
    }
}
