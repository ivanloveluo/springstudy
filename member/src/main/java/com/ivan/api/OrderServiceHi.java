package com.ivan.api;

import com.ivan.hystric.OrderHystric;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author 11900
 * @version V1.0
 * @Title: ${FILE_NAME}
 * @Package com.ivan.api
 * @Description: TODO
 * @date 2019/6/4 12:53
 * Feign中使用断路器
 */
@FeignClient(value = "server-order",fallback = OrderHystric.class)
public interface OrderServiceHi {
    @RequestMapping(value = "/hi",method = RequestMethod.GET)
    String sayHiFromClientOne(@RequestParam(value = "name") String name);
}
