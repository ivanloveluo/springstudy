package com.ivan.hystric;

import com.ivan.api.OrderServiceHi;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author 11900
 * @version V1.0
 * @Title: ${FILE_NAME}
 * @Package com.ivan.hystric
 * @Description: TODO
 * @date 2019/6/4 12:59
 */
@Component
public class OrderHystric implements OrderServiceHi {
    @Override
    public String sayHiFromClientOne(@RequestParam(value = "name") String name) {
        return "sorry "+name+"服务端异常";
    }
}
